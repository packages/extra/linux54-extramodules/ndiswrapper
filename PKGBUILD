# Maintainer: Philip Müller <philm[at]manjaro[dot]org>
# Maintainer: Bernhard Landauer <bernhard@manjaro.org>

_linuxprefix=linux54
_extramodules=extramodules-5.4-MANJARO
pkgbase=ndiswrapper

pkgname=("$_linuxprefix-ndiswrapper")
# Disable the line before and use this to additionally build ndiswraper-utils
# It is needed only once!
#pkgname=("$_linuxprefix-ndiswrapper" "ndiswrapper-utils")

_pkgname=ndiswrapper
groups=("$_linuxprefix-extramodules")
pkgver=1.63
pkgrel=201
pkgdesc="Module for NDIS (Windows Network Drivers) drivers supplied by vendors."
license=('GPL')
arch=('x86_64')
url="http://ndiswrapper.sourceforge.net"
makedepends=("$_linuxprefix-headers" "$_linuxprefix")
source=("https://sourceforge.net/projects/$_pkgname/files/stable/$_pkgname-$pkgver.tar.gz")
options=('!strip')
sha256sums=('0f81ceaac532f7c55e412d0dbdc5d82adb4a07a91000ea240881ea08a531cbec')

prepare() {
  cd "$_pkgname-$pkgver"

  # patches here

}

build() {
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  cd "$_pkgname-$pkgver"

  # modinfo path fix
  sed -i "/modinfo/s/s/usr\//" driver/Makefile

  # make sure we point to the right build directory
  sed -i "/^KBUILD/ s,.*,KBUILD = $(readlink -f /usr/lib/modules/$_kernver/build)," driver/Makefile

  make KVERS=$_kernver
}

package() {
  depends=("$_linuxprefix" "$_pkgname-utils")
  provides=('ndiswrapper-extramodule')
  _kernver="$(cat /usr/lib/modules/${_extramodules}/version)"

  provides=("$_pkgname=$pkgver")
  install=ndiswrapper.install
  cd "$_pkgname-$pkgver"

  make INST_DIR="usr/lib/modules/$_extramodules" \
    KVERS=$_kernver DESTDIR="$pkgdir/" sbindir=/usr/bin usrsbindir=/usr/bin install

  # set the kernel we've built for inside the install script
  sed -i -e "s/EXTRAMODULES=.*/EXTRAMODULES=${_extramodules}/g" "${startdir}/${_pkgname}.install"

  gzip "$pkgdir/usr/lib/modules/$_extramodules/$_pkgname.ko"

  # mv ndiswrapper-utils
  mkdir -p "tmp-ndis-utils/usr"
  mv -v "$pkgdir/usr/share" "tmp-ndis-utils/usr"
  mv -v "$pkgdir/usr/bin" "tmp-ndis-utils/usr"
}

package_ndiswrapper-utils() {
  pkgrel=83
  depends=('wireless_tools' 'perl')
  pkgdesc="Utils for NDIS (Windows Network Drivers) drivers supplied by vendors."
  cd "tmp-ndis-utils"
  mv -v usr "$pkgdir"
}
